# delivery-backend-typescript

This project contains source code and supporting files for a serverless application that you can deploy with the AWS Serverless Application Model (AWS SAM) command line interface (CLI). It includes the following files and folders:

- `src` - Code for the application's Lambda function written in TypeScript.
- `events` - Invocation events that you can use to invoke the function.
- `__tests__` - Unit tests for the application code. 
- `template.yml` - A template that defines the application's AWS resources.

The application uses several AWS resources, including Lambda functions and API Gateway API. These resources are defined in the `template.yml` file in this project. You can update the template to add AWS resources through the same deployment process that updates your application code.

To use the application you need

* AWS SAM CLI - [Install the AWS SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html).
* Node.js - [Install Node.js 14](https://nodejs.org/en/), including the npm package management tool.
* Docker - [Install Docker community edition](https://hub.docker.com/search/?type=edition&offering=community).


## Use the AWS SAM CLI to build and test locally

Copy `env.json.sample` to `env.json` and edit with the dynamodb table name you want to use, then run:

```bash
$ npm install
$ npm run dev
```

## Unit tests

Tests are defined in the `__tests__` folder in this project. Use `npm` to install the [Jest test framework](https://jestjs.io/) and run unit tests.

```bash
$ npm install
$ npm test
```

## Deployment

Deployments are done automatically everytime new changes are done and only to the main branch

Unit tests run, and if they pass the stack is automatically updated in aws

Base url of the backend:
https://8k1fwkbc7b.execute-api.eu-west-1.amazonaws.com/Prod


