import { constructAPIGwEvent } from '../../utils/helpers';
import { getStoreByIdHandler } from '../../../src/handlers/get-store-by-id';
import { DocumentClient } from "aws-sdk/clients/dynamodb";

describe('Test get store by id handler', () => { 
  let querySpy: any;
  beforeAll(() => { 
    // Mock dynamodb get and put methods 
    // https://jestjs.io/docs/en/jest-object.html#jestspyonobject-methodname 
    querySpy = jest.spyOn(DocumentClient.prototype, 'query');
  });
  afterAll(() => { 
    querySpy.mockRestore();
  });  
  
  it('should get retrieve the store from the database when it exists', async () => {
    const store = {
        name: 'testStore',
        openTime: '09:00',
        closeTime: '22:00',
    }
    const mockResultFromQueryDynamo = {
        ...store,
        pk: '__PK__VALUE__',
        sk: '__SK__VALUE__',
        groupType: 'store',
        sortValue: '__SORT_VALUE__'
    }

    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: [mockResultFromQueryDynamo]}) 
    }); 
    const event = constructAPIGwEvent({}, { pathParameters: {id: '__STORE__ID__'},rawBody: '' });
    const result = await getStoreByIdHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(200)
    expect(resultBody.name).toEqual(store.name)
    expect(resultBody.openTime).toEqual(store.openTime)
    expect(resultBody.closeTime).toEqual(store.closeTime)
    expect(resultBody.id).toBeDefined()
    expect(resultBody.pk).toBeUndefined()
    expect(resultBody.sk).toBeUndefined()
    expect(resultBody.groupType).toBeUndefined()
    expect(resultBody.sortValue).toBeUndefined()
  });

  it('should return a not found error when the store does not exist in the database', async () => {
    const store = {
        name: 'testStore',
        openTime: '09:00',
        closeTime: '22:00',
    }
    const mockResultFromQueryDynamo = {
        ...store,
        pk: '__PK__VALUE__',
        sk: '__SK__VALUE__',
        groupType: 'store',
        sortValue: '__SORT_VALUE__'
    }

    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: []}) 
    }); 
    const event = constructAPIGwEvent({}, { pathParameters: {id: '__STORE__ID__'},rawBody: '' });
    const result = await getStoreByIdHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(404)
    expect(resultBody.message).toEqual('Store with id: __STORE__ID__ does not exist')
  });

}); 
