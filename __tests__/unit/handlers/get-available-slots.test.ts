import { constructAPIGwEvent } from '../../utils/helpers';
import { getAvailableSlotsHandler } from '../../../src/handlers/get-available-slots';
import { DocumentClient } from "aws-sdk/clients/dynamodb";

describe('Test get available slots handler', () => { 
  let querySpy: any;
  beforeAll(() => { 
    // Mock dynamodb get and put methods 
    // https://jestjs.io/docs/en/jest-object.html#jestspyonobject-methodname 
    querySpy = jest.spyOn(DocumentClient.prototype, 'query');
  });
  afterAll(() => { 
    querySpy.mockRestore();
  });  
  
  it('should return a valid slot for a store when the store exists and there is capacity', async () => {
    const store = {
        name: 'testStore',
        openTime: '09:00',
        closeTime: '22:00',
    }
    const mockResultFromGetStoreQueryDynamo = {
        ...store,
        pk: '__PK__VALUE__',
        sk: '__SK__VALUE__',
        groupType: 'store',
        sortValue: '__SORT_VALUE__'
    }

    const mockResultFromGetAvailableSlots = {
        pk: 'slot#__PK_VALUE__',
        sk: 'slot#__SK_VALUE__',
        remainingCapacity: 1,
        beginTime: '08:00',
        capacity: 10,
        endTime: '08:30',
        groupType: 'slot',
        sortValue: '08:00'
    }

    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: [mockResultFromGetStoreQueryDynamo]}) 
    });
    
    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: [mockResultFromGetAvailableSlots]}) 
    });
    const event = constructAPIGwEvent({}, { query: {storeId: '__STORE__ID__'} });
    const result = await getAvailableSlotsHandler(event); 
    
    const {pk, sk, groupType, sortValue, ...safeSlotData} = mockResultFromGetAvailableSlots

    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(200)
    expect(resultBody).toEqual([
        {
            id: '__PK_VALUE__',
            ...safeSlotData
        }
    ])
  });

  it('should return a bad request when required query parameter storeId is not passed', async () => {
    const event = constructAPIGwEvent({}, { query: {} });
    const result = await getAvailableSlotsHandler(event); 
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(400)
    expect(resultBody).toEqual({message: 'Required queryString parameter: storeId'})
  });


  it('should return an empty array when the store does not exist', async () => {
    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: []}) 
    });
    const event = constructAPIGwEvent({}, { query: {storeId: '__STORE__ID__'} });
    const result = await getAvailableSlotsHandler(event); 
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(200)
    expect(resultBody).toEqual([])
  });

  it('should return an empty array when the store exists but there are no remaining slots with capacity', async () => {
    const store = {
        name: 'testStore',
        openTime: '09:00',
        closeTime: '22:00',
    }
    const mockResultFromGetStoreQueryDynamo = {
        ...store,
        pk: '__PK__VALUE__',
        sk: '__SK__VALUE__',
        groupType: 'store',
        sortValue: '__SORT_VALUE__'
    }

    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: [mockResultFromGetStoreQueryDynamo]}) 
    });
    
    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: []}) 
    });
    const event = constructAPIGwEvent({}, { query: {storeId: '__STORE__ID__'} });
    const result = await getAvailableSlotsHandler(event); 

    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(200)
    expect(resultBody).toEqual([])
  });

}); 
