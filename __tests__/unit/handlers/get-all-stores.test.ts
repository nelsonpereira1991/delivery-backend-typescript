import { constructAPIGwEvent } from '../../utils/helpers';
import { getAllStoresHandler } from '../../../src/handlers/get-all-stores';
import { DocumentClient } from "aws-sdk/clients/dynamodb";

describe('Test getting all stores handler', () => { 
  let querySpy: any;
  beforeAll(() => { 
    // Mock dynamodb get and put methods 
    // https://jestjs.io/docs/en/jest-object.html#jestspyonobject-methodname 
    querySpy = jest.spyOn(DocumentClient.prototype, 'query');
  });
  afterAll(() => { 
    querySpy.mockRestore();
  });  
  
  it('should get all stores when there are stores in the database', async () => {
    const store = {
        name: 'testStore',
        openTime: '09:00',
        closeTime: '22:00',
    }
    const mockResultFromQueryDynamo = {
        ...store,
        pk: '__PK__VALUE__',
        sk: '__SK__VALUE__',
        groupType: 'store',
        sortValue: '__SORT_VALUE__'
    }

    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: [mockResultFromQueryDynamo]}) 
    }); 
    const event = constructAPIGwEvent({}, { pathParameters: {id: '__STORE__ID__'},rawBody: '' });
    const result = await getAllStoresHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    const receivedStore = resultBody[0]
    expect(result.statusCode).toEqual(200)
    expect(resultBody.length).toEqual(1)
    expect(receivedStore.name).toEqual(store.name)
    expect(receivedStore.openTime).toEqual(store.openTime)
    expect(receivedStore.closeTime).toEqual(store.closeTime)
    expect(receivedStore.id).toBeDefined()
    expect(receivedStore.pk).toBeUndefined()
    expect(receivedStore.sk).toBeUndefined()
    expect(receivedStore.groupType).toBeUndefined()
    expect(receivedStore.sortValue).toBeUndefined()
  });

  it('should return an empty array when there are no stores in the database', async () => {
    const store = {
        name: 'testStore',
        openTime: '09:00',
        closeTime: '22:00',
    }
    const mockResultFromQueryDynamo = {
        ...store,
        pk: '__PK__VALUE__',
        sk: '__SK__VALUE__',
        groupType: 'store',
        sortValue: '__SORT_VALUE__'
    }

    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: []}) 
    }); 
    const event = constructAPIGwEvent({}, { pathParameters: {id: '__STORE__ID__'},rawBody: '' });
    const result = await getAllStoresHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(200)
    expect(resultBody.length).toEqual(0)
  });

}); 
