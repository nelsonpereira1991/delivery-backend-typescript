import { constructAPIGwEvent } from '../../utils/helpers';
import { createNewStoreHandler } from '../../../src/handlers/create-new-store';
import { DocumentClient } from "aws-sdk/clients/dynamodb";

describe('Test create new store handler', () => { 
  let putSpy: any;
  beforeAll(() => { 
    // Mock dynamodb get and put methods 
    // https://jestjs.io/docs/en/jest-object.html#jestspyonobject-methodname 
    putSpy = jest.spyOn(DocumentClient.prototype, 'put'); 
  });
  afterAll(() => { 
    putSpy.mockRestore(); 
  });  
  
  it('should create a store id when the request body is valid', async () => {
    const body = {
        name: 'testStore',
        openTime: '09:00',
        closeTime: '22:00',
    }

    const mockResultFromPutDynamo = {
        ...body,
        pk: '__PK__VALUE__',
        sk: '__SK__VALUE__',
        groupType: 'store',
        sortValue: '__SORT_VALUE__'
    }

    putSpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve(mockResultFromPutDynamo) 
    }); 
    const event = constructAPIGwEvent({}, { rawBody: JSON.stringify(body) });
    const result = await createNewStoreHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(200)
    expect(resultBody.name).toEqual(body.name)
    expect(resultBody.openTime).toEqual(body.openTime)
    expect(resultBody.closeTime).toEqual(body.closeTime)
    expect(resultBody.id).toBeDefined()
    expect(resultBody.pk).toBeUndefined()
    expect(resultBody.sk).toBeUndefined()
    expect(resultBody.groupType).toBeUndefined()
    expect(resultBody.sortValue).toBeUndefined()
  });

  it('should return bad request when missing required field name', async () => {
    const body = {
        openTime: '09:00',
        closeTime: '22:00',
    }

    const mockResultFromPutDynamo = {
        ...body,
        pk: '__PK__VALUE__',
        sk: '__SK__VALUE__',
        groupType: 'store',
        sortValue: '__SORT_VALUE__'
    }

    putSpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve(mockResultFromPutDynamo) 
    }); 
    const event = constructAPIGwEvent({}, { rawBody: JSON.stringify(body) });
    const result = await createNewStoreHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(400)
    expect(resultBody).toEqual({"message": "[{\"instancePath\":\"\",\"schemaPath\":\"#/required\",\"keyword\":\"required\",\"params\":{\"missingProperty\":\"name\"},\"message\":\"must have required property 'name'\"}]"})
  });

  it('should return bad request when missing required field openTime', async () => {
    const body = {
        name: 'testStore',
        closeTime: '22:00',
    }

    const mockResultFromPutDynamo = {
        ...body,
        pk: '__PK__VALUE__',
        sk: '__SK__VALUE__',
        groupType: 'store',
        sortValue: '__SORT_VALUE__'
    }

    putSpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve(mockResultFromPutDynamo) 
    }); 
    const event = constructAPIGwEvent({}, { rawBody: JSON.stringify(body) });
    const result = await createNewStoreHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(400)
    expect(resultBody).toEqual({"message": "[{\"instancePath\":\"\",\"schemaPath\":\"#/required\",\"keyword\":\"required\",\"params\":{\"missingProperty\":\"openTime\"},\"message\":\"must have required property 'openTime'\"}]"})
  });
  
  it('should return bad request when missing required field closeTime', async () => {
    const body = {
        name: 'testStore',
        openTime: '09:00'
    }

    const mockResultFromPutDynamo = {
        ...body,
        pk: '__PK__VALUE__',
        sk: '__SK__VALUE__',
        groupType: 'store',
        sortValue: '__SORT_VALUE__'
    }

    putSpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve(mockResultFromPutDynamo) 
    }); 
    const event = constructAPIGwEvent({}, { rawBody: JSON.stringify(body) });
    const result = await createNewStoreHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(400)
    expect(resultBody).toEqual({"message": "[{\"instancePath\":\"\",\"schemaPath\":\"#/required\",\"keyword\":\"required\",\"params\":{\"missingProperty\":\"closeTime\"},\"message\":\"must have required property 'closeTime'\"}]"})
  });

  it('should return bad request when having any extra request attribute in the body', async () => {
    const body = {
        name: 'testStore',
        openTime: '09:00',
        closeTime: '22:00',
        extraAttribute: 'extraAttribute'
    }

    const mockResultFromPutDynamo = {
        ...body,
        pk: '__PK__VALUE__',
        sk: '__SK__VALUE__',
        groupType: 'store',
        sortValue: '__SORT_VALUE__'
    }

    putSpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve(mockResultFromPutDynamo) 
    }); 
    const event = constructAPIGwEvent({}, { rawBody: JSON.stringify(body) });
    const result = await createNewStoreHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(400)
    expect(resultBody).toEqual({"message": "[{\"instancePath\":\"\",\"schemaPath\":\"#/additionalProperties\",\"keyword\":\"additionalProperties\",\"params\":{\"additionalProperty\":\"extraAttribute\"},\"message\":\"must NOT have additional properties\"}]"})
  });
}); 
