import { constructAPIGwEvent } from '../../utils/helpers';
import { createNewOrderHandler } from '../../../src/handlers/create-new-order';
import { DocumentClient } from "aws-sdk/clients/dynamodb";

describe('Test create new order handler', () => { 
  let updateSpy: any;
  let querySpy: any;
  beforeAll(() => { 
    // Mock dynamodb get and put methods 
    // https://jestjs.io/docs/en/jest-object.html#jestspyonobject-methodname 
    querySpy = jest.spyOn(DocumentClient.prototype, 'query');
    updateSpy = jest.spyOn(DocumentClient.prototype, 'update');
  });
  afterAll(() => { 
    querySpy.mockRestore();
    updateSpy.mockRestore();
  });  
  
  it('should create a new order when the request body is valid and there is remaining capacity', async () => {
    const body = {
        id: '__SLOT_ID__'
    }

    const mockResultFromQueryDynamo = {
        pk: 'slot#__PK_VALUE__',
        sk: 'slot#__SK_VALUE__',
        remainingCapacity: 1,
        beginTime: '08:00',
        capacity: 10,
        endTime: '08:30',
        groupType: 'slot',
        sortValue: '08:00'
    }

    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: [mockResultFromQueryDynamo]}) 
    }); 
    updateSpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve(true) 
    }); 
    const event = constructAPIGwEvent({}, { rawBody: JSON.stringify(body) });
    const result = await createNewOrderHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(200)
    expect(resultBody).toEqual(true)
  });

  it('should not create a new order when the request body is valid but there is not remaining capacity for the slot', async () => {
    const body = {
        id: '__SLOT_ID__'
    }

    const mockResultFromQueryDynamo = {
        pk: 'slot#__PK_VALUE__',
        sk: 'slot#__SK_VALUE__',
        remainingCapacity: 0,
        beginTime: '08:00',
        capacity: 10,
        endTime: '08:30',
        groupType: 'slot',
        sortValue: '08:00'
    }

    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: [mockResultFromQueryDynamo]}) 
    }); 
    updateSpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve(true) 
    }); 
    const event = constructAPIGwEvent({}, { rawBody: JSON.stringify(body) });
    const result = await createNewOrderHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(400)
    expect(resultBody).toEqual({message: 'No slots available'})
  });

  it('should not create a new order when the request body is valid but the slot does not exist', async () => {
    const body = {
        id: '__SLOT_ID__'
    }

    const mockResultFromQueryDynamo = {
        pk: 'slot#__PK_VALUE__',
        sk: 'slot#__SK_VALUE__',
        remainingCapacity: 0,
        beginTime: '08:00',
        capacity: 10,
        endTime: '08:30',
        groupType: 'slot',
        sortValue: '08:00'
    }

    querySpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve({Items: []}) 
    }); 
    updateSpy.mockReturnValueOnce({ 
        promise: () => Promise.resolve(true) 
    }); 
    const event = constructAPIGwEvent({}, { rawBody: JSON.stringify(body) });
    const result = await createNewOrderHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(400)
    expect(resultBody).toEqual({message: 'No slots available'})
  });

  it('should return a bad request when the request body is not valid', async () => {
    const body = {} 
    const event = constructAPIGwEvent({}, { rawBody: JSON.stringify(body) });
    const result = await createNewOrderHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(400)
    expect(resultBody).toEqual({message: "[{\"instancePath\":\"\",\"schemaPath\":\"#/required\",\"keyword\":\"required\",\"params\":{\"missingProperty\":\"id\"},\"message\":\"must have required property 'id'\"}]"})
  });

  it('should return a bad request when the request body has extra fields', async () => {
    const body = {
        id: '__SLOT_ID__',
        extraField: 'this is an extra field oh nooooo'
    }
    const event = constructAPIGwEvent({}, { rawBody: JSON.stringify(body) });
    const result = await createNewOrderHandler(event); 
    
    const resultBody = JSON.parse(result.body)
    expect(result.statusCode).toEqual(400)
    expect(resultBody).toEqual({message: "[{\"instancePath\":\"\",\"schemaPath\":\"#/additionalProperties\",\"keyword\":\"additionalProperties\",\"params\":{\"additionalProperty\":\"extraField\"},\"message\":\"must NOT have additional properties\"}]"})
  });
}); 
