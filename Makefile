.PHONY: build-RuntimeDependenciesLayer build-lambda-common
.PHONY: build-getAllStoresFunction
.PHONY: build-getStoreByIdFunction
.PHONY: build-getAvailableSlotsFunction
.PHONY: build-createNewStoreFunction
.PHONY: build-updateStoreFunction
.PHONY: build-createNewOrderFunction


build-createNewOrderFunction:
	$(MAKE) HANDLER=src/handlers/create-new-order.ts build-lambda-common

build-updateStoreFunction:
	$(MAKE) HANDLER=src/handlers/update-store.ts build-lambda-common

build-createNewStoreFunction:
	$(MAKE) HANDLER=src/handlers/create-new-store.ts build-lambda-common

build-getAvailableSlotsFunction:
	$(MAKE) HANDLER=src/handlers/get-available-slots.ts build-lambda-common

build-getStoreByIdFunction:
	$(MAKE) HANDLER=src/handlers/get-store-by-id.ts build-lambda-common

build-getAllStoresFunction:
	$(MAKE) HANDLER=src/handlers/get-all-stores.ts build-lambda-common

build-lambda-common:
	npm install
	rm -rf dist
	echo "{\"extends\": \"./tsconfig.json\", \"include\": [\"${HANDLER}\"] }" > tsconfig-only-handler.json
	npm run build -- --build tsconfig-only-handler.json
	cp -r dist "$(ARTIFACTS_DIR)/"

build-RuntimeDependenciesLayer:
	mkdir -p "$(ARTIFACTS_DIR)/nodejs"
	cp package.json package-lock.json "$(ARTIFACTS_DIR)/nodejs/"
	npm install --production --prefix "$(ARTIFACTS_DIR)/nodejs/"
	rm "$(ARTIFACTS_DIR)/nodejs/package.json" # to avoid rebuilding when changes aren't related to dependencies
