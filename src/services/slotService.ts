import { getAvailableSlotsQuery } from "../db/repo/slots"
import { getStoreByIdQuery } from "../db/repo/stores"
import {Slot} from '../types/types'

export const getAvailableSlotsForStore = async(storeId: string) : Promise<Slot[]> => {
    const store = await getStoreByIdQuery(storeId)
    if(store) {
        const {openTime, closeTime} = store
        const availableSlots = await getAvailableSlotsQuery(openTime, closeTime)
        return availableSlots
    } else {
        return []
    }
}