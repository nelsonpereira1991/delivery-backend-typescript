import {CreateStoreRequest, Store, UpdateStoreRequest} from '../types/types'
import { createStoreQuery, getStoreByIdQuery, getStoresQuery, updateStoreQuery } from "../db/repo/stores";


export const createStore = (store: CreateStoreRequest): Promise<Store> => (
    createStoreQuery(store)
)

export const getStore = async(id: string): Promise<Store | null> => (
    getStoreByIdQuery(id)
)

export const getStores = () : Promise<Store[]> => (
    getStoresQuery()
)

export const updateStore = async(store: UpdateStoreRequest, id: string) : Promise<Store | null> => {
    const storeExists = await getStoreByIdQuery(id)
    console.log('store exist is: ', storeExists)
    if(!storeExists) {
        console.log('will return null')
        return null
    }
    return updateStoreQuery(store, id)
}