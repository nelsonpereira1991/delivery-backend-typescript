import { getSlotByIdQuery, updateSlotRemainingCapacityQuery } from "../db/repo/slots"
import {Slot} from '../types/types'


export const createOrder = async(id: string): Promise<boolean> => {
    const slot: Slot = await getSlotByIdQuery(id)
    if(!!slot && slot.remainingCapacity > 0) {
        const newRemainingCapacity = slot.remainingCapacity - 1
        return updateSlotRemainingCapacityQuery(id, newRemainingCapacity)
    } else {
        return false
    }
}

