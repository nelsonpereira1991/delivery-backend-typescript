export default {
    type: "object",
    properties: {
        name: {type: "string", "minLength": 2},
        openTime: {type: "string", "pattern": "^([0-1][0-9]|2[0-3]):[0-5][0-9]$"},
        closeTime: {type: "string", "pattern": "^([0-1][0-9]|2[0-3]):[0-5][0-9]$"}
    },
    required: ["name", "openTime", "closeTime"],
    additionalProperties: false,
}