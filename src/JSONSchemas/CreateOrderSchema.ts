export default {
    type: "object",
    properties: {
        id: {type: "string", minLength: 3},
    },
    required: ["id"],
    additionalProperties: false,
}