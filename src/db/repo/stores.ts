import {Store, StoreDbItem, CreateStoreRequest, UpdateStoreRequest} from '../../types/types'
import { generateId } from "../../utils/generateId";
import docClient from "../client/dbClient";
const tableName = process.env.DATABASE_TABLE as string;

const buildStoresEntities = (storesDbItems: StoreDbItem[]): Store[] => (
  storesDbItems.map((storeDbItem: StoreDbItem) => (
    buildStoreEntity(storeDbItem)
  ))
)

const buildStoreEntity = (storeDbItem: StoreDbItem): Store => {
  if(!!storeDbItem) {
    const {pk, sk, groupType, sortValue, ...store} = storeDbItem
    return {
      id: pk.replace('store#', ''),
      ...store
    }
  }
  return storeDbItem
}

export const createStoreQuery = async(store: CreateStoreRequest): Promise<Store> => {
    console.log('storeData is: ', store)
    const id = generateId()
    const newStore = {
        pk: `store#${id}`,
        sk: `store#${id}`,
        groupType: 'store',
        sortValue: 'store',
        ...store
    }

    const params = {
        TableName: tableName,
        Key: {
        pk: `store#${id}`,
        sk: `store#${id}`,
        },
        Item: newStore,
    }

    try {
        console.log('Attempting to execute store creation query.', JSON.stringify(params))

        await docClient()
        .put(params)
        .promise()

        return {
          ...store,
          id
        }
    } catch (e) {
        console.error('Error while creating store', e)
        throw e
    }
}

export const getStoreByIdQuery = async(storeID: string): Promise<Store> => {
    console.log('table name is: ', tableName)
    const params = {
        TableName: tableName,
        KeyConditionExpression: `pk = :pk`,
        ExpressionAttributeValues: {
        ':pk': `store#${storeID}`,
        },
    }

    try {
        console.log(`Attempting to fetch store with id ${storeID}`)

        const items = (
        await docClient()
            .query(params)
            .promise()
        ).Items

        const storeDbItem = items?.length ? items[0] : null
        return buildStoreEntity(storeDbItem as StoreDbItem)
    } catch (e) {
        console.error(`Error while fetching store with id ${storeID}`, e)
        throw e
    }

}

export const getStoresQuery = async() : Promise<Store[]> => {
    const params = {
        TableName: tableName,
        IndexName: 'groupTypeIndex',
        KeyConditionExpression: `groupType = :v_groupType`,
        ExpressionAttributeValues: {
            ':v_groupType': 'store',
        },
    }
      
    try {
        console.log('Attempting to fetch all stores')
        
        const items = (
            await docClient()
            .query(params)
            .promise()
        ).Items
        
        const storesDBItems =  items?.length ? items : []
        return buildStoresEntities(storesDBItems as StoreDbItem[])
    } catch (e) {
        console.error('Error while fetching stores', e)
        throw e
    }
}


export const updateStoreQuery = async(store: UpdateStoreRequest, id: string): Promise<Store>  => {
    console.log('storeData is: ', store)
    const newStore = {
      pk: `store#${id}`,
      sk: `store#${id}`,
      groupType: 'store',
      sortValue: 'store',
      ...store
    }
    
    const params = {
      TableName: tableName,
      Key: {
        pk: `store#${id}`,
        sk: `store#${id}`,
      },
      Item: newStore,
    }
  
    try {
      console.log('Attempting to execute store update query.', JSON.stringify(params))
    
      await docClient()
        .put(params)
        .promise()
    
      return {
        ...store,
        id
      }
    } catch (e) {
      console.error('Error while updating store', e)
      throw e
    }
  }