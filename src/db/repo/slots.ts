import docClient from "../client/dbClient";
import {Slot, SlotDbItem} from '../../types/types'
const tableName = process.env.DATABASE_TABLE as string;


const buildSlotsEntities = (slotsDbItems: SlotDbItem[]) : Slot[] => (
  slotsDbItems.map((slotDbItem) => (
    buildSlotEntity(slotDbItem)
  ))
)

const buildSlotEntity = (slotDbItem: SlotDbItem): Slot => {
  if(!!slotDbItem) {
    const {pk, sk, groupType, sortValue, ...slot} = slotDbItem
    return {
      id: pk.replace('slot#', ''),
      ...slot
    }
  }
  return slotDbItem
}

export const getAvailableSlotsQuery = async(openTime: string, closeTime: string): Promise<Slot[]> => {
    console.log('table name is debug: ', tableName)
    console.log('the open time is: ', openTime)
    console.log('the close time is: ', closeTime)

    const params = {
      TableName: tableName,
      IndexName: 'groupTypeIndex',
      KeyConditionExpression: `groupType = :v_groupType`,
      FilterExpression: `remainingCapacity > :value AND beginTime >= :beginValue AND endTime < :endValue`,
      ExpressionAttributeValues: {
          ':v_groupType': 'slot',
          ':beginValue': openTime,
          ':endValue': closeTime,
          ':value': 0
      },
    }
    
    try {
      console.log('Attempting to fetch available slots')
    
      const items = (
        await docClient()
          .query(params)
          .promise()
      ).Items
      const slotsDbItems =  items?.length ? items : []
      return buildSlotsEntities(slotsDbItems as SlotDbItem[])
    } catch (e) {
      console.error('Error while fetching available slots', e)
      throw e
    }
    
}


export const getSlotByIdQuery = async(slotId: string): Promise<Slot> => {
  console.log('table name is: ', tableName)
  const params = {
    TableName: tableName,
    KeyConditionExpression: `pk = :pk`,
    ExpressionAttributeValues: {
      ':pk': `slot#${slotId}`,
    },
  }
  
  try {
    console.log(`Attempting to fetch slot with id ${slotId}`)
  
    const items = (
      await docClient()
        .query(params)
        .promise()
    ).Items
  
    const slotDbItem = items?.length ? items[0] : null
    return buildSlotEntity(slotDbItem as SlotDbItem)
  } catch (e) {
    console.error(`Error while fetching slot with id ${slotId}`, e)
    throw e
  }
}

export const updateSlotRemainingCapacityQuery = async(id: string, newRemainingCapacity: number): Promise<boolean> => {
    console.log('newRemainingCapacity is: ', newRemainingCapacity)
    const params = {
      TableName: tableName,
      Key: {
        pk: `slot#${id}`,
        sk: `slot#${id}`,
      },
      UpdateExpression: "set remainingCapacity = :r",
      ExpressionAttributeValues:{
        ":r":newRemainingCapacity
      },
      ReturnValues:"UPDATED_NEW"
    }
  
    try {
      console.log('Attempting to update slot remaining capacity', JSON.stringify(params))
    
      await docClient()
        .update(params)
        .promise()
    
      return true
    } catch (e) {
      console.error('Error while updating a Slot remaining capacity', e)
      throw e
    }
}