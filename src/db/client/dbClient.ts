import { DocumentClient } from "aws-sdk/clients/dynamodb";

let dynamo: DocumentClient | null = null

const initClient = () => {
    dynamo = new DocumentClient({
        apiVersion: '2012-08-10',
        convertEmptyValues: false,
    });

    return dynamo
}

export default (): DocumentClient => dynamo || initClient()