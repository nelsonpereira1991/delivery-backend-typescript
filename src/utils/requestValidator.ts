import Ajv from "ajv"
import { CreateOrderSchema, CreateStoreSchema, UpdateStoreSchema } from "../JSONSchemas"
import {CreateStoreRequest, UpdateStoreRequest} from '../types/types'
const ajv = new Ajv()

type ValidationResult = {
    result: boolean
    message: string
}

const validateSchema = (schema: any, data: any): ValidationResult => {
    const validate = ajv.compile(schema)
    const valid = validate(data)
    let message = 'success'
    if (!valid) {
        message = JSON.stringify(validate.errors)
    }
    return {
        result: valid,
        message
    }
}

export const validateCreateStoreRequest = (data: CreateStoreRequest) => {
    return validateSchema(CreateStoreSchema, data)
}

export const validateUpdateStoreRequest = (data: UpdateStoreRequest) => {
    return validateSchema(UpdateStoreSchema, data)
}

export const validateCreateOrderRequest = (data: any) => {
    return validateSchema(CreateOrderSchema, data)
}