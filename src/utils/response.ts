import {
    APIGatewayProxyResult
  } from 'aws-lambda';

const createResponse = (statusCode: number, body: any): APIGatewayProxyResult => {
    const response = {
        statusCode: statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*', 
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    };
    return response
}

export const successResponse = (resource: any) => {
    return createResponse(200, resource)
}

export const notFoundResponse = (message: string) => {
    return createResponse(404, {message})
}

export const badRequestResponse = (message: string) => {
    return createResponse(400, {message})
}

export const internalServerErrorResponse = (message?: string) => {
    return createResponse(500, {message: message ? message: 'Internal server error!'})
}