export type Store = {
    id: string
    name: string
    openTime: string
    closeTime: string
}


export type Slot = {
    id: string
    remainingCapacity: number
    beginTime: string
    capacity: number
    endTime: string
}



//DB types
export type DbItem = {
    pk: string
    sk: string
    groupType: string
    sortValue: string
}

export type SlotDbItem = Omit<Slot, 'id'> & DbItem
export type StoreDbItem = Omit<Store, 'id'> & DbItem



//Requests 
export type CreateStoreRequest = Omit<Store, 'id'>
export type UpdateStoreRequest = Omit<Store, 'id'>