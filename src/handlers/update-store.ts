import 'source-map-support/register';
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  APIGatewayProxyEventPathParameters
} from 'aws-lambda';

import { notFoundResponse, successResponse, badRequestResponse } from '../utils/response';
import { updateStore } from '../services/storeService';
import {UpdateStoreRequest} from '../types/types'
import { validateUpdateStoreRequest } from '../utils/requestValidator';

/**
 * A simple example includes a HTTP post method to add one item to a DynamoDB table.
 */
export const updateStoreHandler = async (
   event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
    // Get id and name from the body of the request
    const {id} = event.pathParameters as APIGatewayProxyEventPathParameters;
    const body = JSON.parse(event.body as string)
    
    //name,  openTime, closeTime
    const store: UpdateStoreRequest = body
    const validationResult = validateUpdateStoreRequest(store)
    if(validationResult.result === false) {
      return badRequestResponse(validationResult.message)
    }
    const updatedStore = await updateStore(store, id as string)
    if(!updatedStore){
      console.log('will throw a not found store')
      return notFoundResponse(`Store with id: ${id} does not exist`)
    }
    const response = successResponse(updatedStore)
    return response;
}
