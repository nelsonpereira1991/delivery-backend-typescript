import 'source-map-support/register';
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  APIGatewayProxyEventPathParameters
} from 'aws-lambda';
import { getStore } from "../services/storeService";
import { notFoundResponse, successResponse } from "../utils/response";
import {Store} from '../types/types'


/**
 * A simple example includes a HTTP get method to get one item by id from a DynamoDB table.
 */
export const getStoreByIdHandler = async (
    event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
  const {id} = event.pathParameters as APIGatewayProxyEventPathParameters;
  const store: Store | null = await getStore(id as string)
  if(!store) {
    return notFoundResponse(`Store with id: ${id} does not exist`)
  }
  const response = successResponse(store)
  return response;
}
