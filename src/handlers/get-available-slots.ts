import 'source-map-support/register';
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  APIGatewayProxyEventQueryStringParameters
} from 'aws-lambda';

import { getAvailableSlotsForStore } from "../services/slotService";
import { badRequestResponse, successResponse } from "../utils/response";


/**
 * A simple example includes a HTTP get method to get all items from a DynamoDB table.
 */
export const getAvailableSlotsHandler = async (
    event: APIGatewayProxyEvent
 ): Promise<APIGatewayProxyResult> => {
    console.log('query string parameters is: ', event.queryStringParameters)
    const storeId = event?.queryStringParameters?.storeId ?? null
    if(!storeId){
      return badRequestResponse('Required queryString parameter: storeId')
    }
    const availableSlots = await getAvailableSlotsForStore(storeId as string)
    const response = successResponse(availableSlots)

    return response;
}
