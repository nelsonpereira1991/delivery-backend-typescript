import 'source-map-support/register';
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda';

import {successResponse} from '../utils/response'
import { getStores } from '../services/storeService';
import {Store} from '../types/types'

/**
 * A simple example includes a HTTP get method.
 */
export const getAllStoresHandler = async (
  event: APIGatewayProxyEvent,
): Promise<APIGatewayProxyResult> => {
  // All log statements are written to CloudWatch
  //console.debug('Received event:', event);
  const stores: Store[] = await getStores()
  const response = successResponse(stores)
  return response
}
