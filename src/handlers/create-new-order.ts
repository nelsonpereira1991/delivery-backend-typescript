import 'source-map-support/register';
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda';

import { createOrder } from "../services/orderService";
import { badRequestResponse, successResponse } from "../utils/response";
import { validateCreateOrderRequest } from '../utils/requestValidator';

/**
 * A simple example includes a HTTP post method to add one item to a DynamoDB table.
 */
export const createNewOrderHandler = async (
   event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
    const body = JSON.parse(event.body as string)
    const validationResult = validateCreateOrderRequest(body)
    if(validationResult.result === false) {
      return badRequestResponse(validationResult.message)
    }
    const {id} = body
    const result = await createOrder(id)
    let response: APIGatewayProxyResult
    if(result === true) {
        response = successResponse(result)
    } else {
        response = badRequestResponse('No slots available')
    }
    return response;
}
