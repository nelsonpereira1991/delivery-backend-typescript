import 'source-map-support/register';
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda';

import { createStore } from '../services/storeService';
import { badRequestResponse, successResponse } from '../utils/response';
import {CreateStoreRequest} from '../types/types'
import { validateCreateStoreRequest } from '../utils/requestValidator';


/**
 * A simple example includes a HTTP post method to add one item to a DynamoDB table.
 */
export const createNewStoreHandler = async (
   event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
    const body = JSON.parse(event.body as string)
    
    const store: CreateStoreRequest = body
    const validationResult = validateCreateStoreRequest(store)
    if(validationResult.result === false) {
      return badRequestResponse(validationResult.message)
    }
    const createdStore = await createStore(store)
    const response = successResponse(createdStore)

    // All log statements are written to CloudWatch
    console.info(`response from: ${event.path} statusCode: ${response.statusCode} body: ${response.body}`);
    return response;
}
